cmake_minimum_required(VERSION 3.26)
project(Incision VERSION 0.1.0)


set(CMAKE_EXPORT_COMPILE_COMMANDS True)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)


add_executable(incision src/main.cpp src/iudp.cpp)

target_compile_definitions(incision PRIVATE
	#$<$<CONFIG:Debug>:DEBUG_BUILD BOOST_ASIO_ENABLE_HANDLER_TRACKING> 
	BOOST_ASIO_HAS_CO_AWAIT
	BOOST_LOG_DYN_LINK
	BOOST_ALL_DYN_LINK
)
target_include_directories(incision PUBLIC ${PROJECT_SOURCE_DIR}/include)
target_link_libraries(incision boost_system boost_program_options cryptopp)


