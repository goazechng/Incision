#include <iostream>
#include <boost/program_options.hpp>

#include "udp_test.hpp"

using namespace std;

void startup(const int args, const char* argv[])
{
	namespace po = boost::program_options;
	string desc_text{"Usage: incision [options]\nOptions"};
	po::options_description desc{desc_text.c_str()};
	desc.add_options()
		("help", "show this help.")
		("version", "print version and other info.")
		("udp-test-port", po::value<uint16_t>(), "")
		("udp-test-address", po::value<string>(), "")
		("udp-test-count", po::value<size_t>(), "")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(args, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<endl;
	}
	else if(vm.count("version"))
	{
		cout<<"DEBUG VERSION"<<endl;
	}
	else if(vm.count("udp-test-port")&&vm.count("udp-test-address"))
	{
		size_t count = 10;
		if(vm.count("udp-test-count"))
		{
			count = vm["udp-test-count"].as<size_t>();
		}
		incision::udp_test t{vm["udp-test-address"].as<string>(), 
				vm["udp-test-port"].as<uint16_t>(), count};
	}
	else if(vm.count("udp-test-address")==0&&vm.count("udp-test-port"))
	{
		incision::udp_test t{vm["udp-test-port"].as<uint16_t>()};	
	}

	return;
}


int main(const int args, const char* argv[])
{
	try
	{
		startup(args, argv);
	}
	catch(exception& e)
	{
		cout<<"Exception: "<<e.what()<<endl;
		return -1;
	}
	return 0;
}
