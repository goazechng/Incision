#include "iudp.h"

#include <chrono>
#include <array>
#include <string_view>
#include <queue>
#include <map>
#include <bit>

#include <boost/asio/ip/udp.hpp>

#include <cryptopp/chacha.h>
#include <cryptopp/osrng.h>


namespace incision {
using namespace std;
namespace asio = boost::asio;
namespace ip = boost::asio::ip;

using byte_view = std::basic_string_view<unsigned char>;
using byte_string = std::basic_string<unsigned char>;

class xchacha20_processor
{
	CryptoPP::AutoSeededRandomPool prng;
	
	CryptoPP::SecByteBlock key{32};
	CryptoPP::XChaCha20::Encryption enc;
	
	CryptoPP::XChaCha20::Decryption dec;
public:

	void set_decryption_key(const array<unsigned char, 32>& key_array)
	{
		CryptoPP::SecByteBlock key{32};
		memmove(key.data(), key_array.data(), key_array.size());
		dec.SetKey(key, key.size());
	}

	void set_decryption_iv(const array<unsigned char, 24>& iv_array)
	{
		CryptoPP::SecByteBlock iv{24};
		memmove(iv.data(), iv_array.data(), iv_array.size());
		dec.Resynchronize(iv, iv.size());	
	}
	
	array<unsigned char, 24> reset_encryption_iv()
	{
		CryptoPP::SecByteBlock iv{24};
		prng.GenerateBlock(iv, iv.size());
		enc.Resynchronize(iv);
		array<unsigned char, 24> ret;
		memmove(ret.data(), iv.data(), iv.size());
		return ret;
	}

	void random_encryption_key()
	{
		prng.GenerateBlock(key, key.size());
		enc.SetKey(key, key.size());
	}
	xchacha20_processor()
	{
		random_encryption_key();	
	}
	array<unsigned char, 32> get_encryption_key()
	{
		array<unsigned char, 32> ret;
		memmove(ret.data(), key.data(), key.size());
		return ret;
	}


	byte_string encrypt(const byte_view src)
	{
		byte_string dst;
		dst.resize(src.size());
		enc.ProcessData(reinterpret_cast<unsigned char*>(dst.data()), 
					reinterpret_cast<const unsigned char*>(src.data()), 
					src.size());
		return dst;
	}
	byte_string decrypt(const byte_view src)
	{
		byte_string dst;
		dst.resize(src.size());
		dec.ProcessData(reinterpret_cast<unsigned char*>(dst.data()), 
					reinterpret_cast<const unsigned char*>(src.data()), 
					src.size());
		return dst;
	}

};

template<typename T>
T net_to_native(byte_view bv)
{
	assert(sizeof(T)==bv.size());
	T t=0;
	unsigned char* p = reinterpret_cast<unsigned char*>(&t);
	if constexpr (endian::native == endian::big)
	{
		for(size_t i=0; i<sizeof(T); i++)
		{
			p[i]=bv[i];	
		}
	}
	else
	{
		for(size_t i=0; i<sizeof(T); i++)
		{
			p[i]=bv[sizeof(T)-i-1];	
		}
	}
	return t;
}
template<typename T>
byte_string native_to_net(T t)
{
	byte_string bs;
	bs.resize(sizeof(T));
	unsigned char* p = reinterpret_cast<unsigned char*>(&t);
	if constexpr (endian::native == endian::big)
	{
		for(size_t i=0; i<sizeof(T); i++)
		{
			bs[i]=p[i];
		}
	}
	else
	{
		for(size_t i=0; i<sizeof(T); i++)
		{
			bs[i]=p[sizeof(T)-i-1];
		}
	}
	return bs;
}
/*
 *	
 *	iudp:
 *  | remote_ident(8) | iv(24) | cdata |
 * 
 *	cdata:
 *	| remote_ident(8) | local_ident(8) | index(4) | adata | 
 *
 */

class connection
{
public:
	using timepoint = chrono::time_point<chrono::system_clock, chrono::milliseconds>;
	
	static uint64_t cast_to_ident(byte_view v)
	{
		return net_to_native<uint64_t>(v);
	}
	void push_iudp(byte_string data)
	{
		if(iudp_queue.size()>max_iudp)
		{
			//report
			return;
		}
		iudp_queue.push(std::move(data));
	}
private:
	bool process_one()
	{
		if(iudp_queue.empty())
			return false;
		const byte_string bs = iudp_queue.front();
		iudp_queue.pop();

		if(bs.size()<=64)
			return true;

		//

		return true;
	}

private:

	xchacha20_processor processor;
	
	enum { max_iudp = 4096 };
	queue<byte_string> iudp_queue;
	
	struct cdata
	{
		uint64_t remote_ident;
		uint64_t local_ident;
		uint32_t index;
		byte_string adata;
	};	
};

class iudp_engine_impl
{
	asio::io_context ctx;
	map<uint64_t, connection> connections_map;

public:
	iudp_engine_impl(uint16_t port)
	{

	}



	bool try_push(byte_view data)
	{
		assert(data.size()>=sizeof(uint64_t));
		uint64_t ident = connection::cast_to_ident(data.substr(0,8));
		auto cit = connections_map.find(ident);
		if(cit == connections_map.end())
			return false;
	
		cit->second.push_iudp(byte_string{data});
		return true;
	}
};

class udp_server
{
public:
	udp_server(iudp_engine_impl* engine,
			asio::io_context& ctx,
			uint16_t port = 0)
		: engine{engine} 
		, main_socket{ctx, ip::udp::endpoint(ip::udp::v6(), port)}
	{
		do_receive();
	}

	uint16_t receive_port()
	{
		return main_socket.local_endpoint().port();
	}

public:
	void do_receive()
	{
		using namespace std::placeholders;
		main_socket.async_receive_from(
			asio::buffer(buf, buf.size()),
			curr_sender,
			bind(&udp_server::do_receive_done, this, _1, _2));	
	}
public:
	void do_receive_done(boost::system::error_code ec, size_t bytes_recvd)
	{
		if(!ec&&bytes_recvd>min_length)
		{
			if(engine->try_push(byte_view{buf.data(), bytes_recvd}))
			{
				do_receive();
			}
			else
			{
				do_receive();
			}
		}
		else
		{
			//report error
			do_receive();
		}
	}
private:
	iudp_engine_impl* engine;
	ip::udp::socket main_socket;

	ip::udp::endpoint curr_sender;
	enum { max_length = 65536, min_length = 64 };
	array<unsigned char, max_length> buf;
};



/* namespace incision */ }
