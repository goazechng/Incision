#ifndef INCISION_UDP_TEST_HPP
#define INCISION_UDP_TEST_HPP
#include <iostream>
#include <sstream>
#include <chrono>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/udp.hpp>
namespace incision{

using namespace std;
namespace asio = boost::asio;
namespace ip = boost::asio::ip;

class udp_test
{
private:
	bool server_mode = false;
	asio::io_context ctx;

	string time_now()
	{
		using ms_timepoint =  chrono::sys_time<chrono::milliseconds>;
		ms_timepoint p = time_point_cast<chrono::milliseconds>(chrono::system_clock::now());
		long long t = p.time_since_epoch().count();
		return to_string(t);
	}
public:
	udp_test(uint16_t port)
		: server_mode{true}
	{
		ip::udp::socket s{ctx, ip::udp::endpoint{ip::udp::v6(), port}};
		for(;;)
		{
			string msg;
			msg.resize(128);
			ip::udp::endpoint sender_ep;
			size_t len = s.receive_from(asio::buffer(msg), sender_ep);
			string now = time_now();
			msg.resize(len);
			msg.append("/");
			msg.append(now);
			msg.append("=");
			
			string header;
			size_t all;
			size_t id;
			long long t1;
			char x;
			long long t2;
			istringstream istr{msg};
			istr>>header>>all>>id>>t1>>x>>t2>>x;

			msg.append(to_string(t2-t1));
			
			cout<<msg<<endl;
		}
	}
	udp_test(string address, uint16_t port, size_t count = 10)
	{
		ip::udp::socket s{ctx, ip::udp::endpoint{ip::udp::v6(),0}};
		ip::udp::resolver resolver{ctx};
		boost::system::error_code ec;
		ip::address addr = ip::make_address(address, ec);
		if(ec)
		{
			addr = resolver.resolve(address, "")->endpoint().address();
		}
		ip::udp::endpoint ep{addr, port};
		
		size_t all = count;
		string msg_header{"UDPPING "};
		msg_header.append(to_string(all));
		
		for(size_t i=0; i<all; i++)
		{
			string msg{msg_header};
			msg.push_back(' ');
			msg.append(to_string(i));
			msg.push_back(' ');
			msg.append(time_now());
			s.send_to(asio::buffer(msg), ep);
		}
		
	}
};


/* namespace incision */}

#endif
