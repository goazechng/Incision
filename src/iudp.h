#ifndef INCISION_IUDP_H
#define INCISION_IUDP_H
#include <string>
#include <memory>
namespace incision{

class iudp_engine_impl;
class iudp_engine
{
	std::unique_ptr<iudp_engine_impl> impl;
public:
	iudp_engine(uint16_t port);
	void run();
};

class iudp_stream
{
public:

};


/* namespace incision */ }

#endif
